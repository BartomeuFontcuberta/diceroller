package cat.itb.diceroller

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlin.random.Random
import android.view.Gravity
import android.view.View
import android.view.animation.AnimationUtils
import com.google.android.material.snackbar.Snackbar
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import com.google.android.material.internal.ContextUtils.getActivity
import com.google.android.material.snackbar.BaseTransientBottomBar


class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    lateinit var resetButton: Button
    lateinit var dice1: ImageView
    lateinit var dice2: ImageView
    val daus= arrayOf(
        R.drawable.dice_1,
        R.drawable.dice_2,
        R.drawable.dice_3,
        R.drawable.dice_4,
        R.drawable.dice_5,
        R.drawable.dice_6
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rollButton = findViewById(R.id.roll_button)
        resetButton = findViewById(R.id.reset_button)
        dice1 = findViewById(R.id.dice_1)
        dice2 = findViewById(R.id.dice_2)

        rollButton.setOnClickListener {
            girar(dice1)
            girar(dice2)
            var jackpot=tirada(dice1)
            jackpot= tirada(dice2)&&jackpot
            if (jackpot){
                val mySnackbar = Snackbar.make(rollButton, "JACKPOT!", 2000)
                val view = mySnackbar.view
                val params = view.layoutParams as FrameLayout.LayoutParams
                params.gravity = Gravity.TOP
                view.layoutParams = params
                mySnackbar.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
                mySnackbar.view.setBackgroundColor(Color.parseColor("#FF0000"))
                mySnackbar.setTextColor(Color.parseColor("#FFFFFF"))
                mySnackbar.show()
            }
        }
        resetButton.setOnClickListener{
            reset(dice1)
            reset(dice2)
        }
        dice1.setOnClickListener{
            girar(dice1)
            tirada(dice1)
        }
        dice2.setOnClickListener {
            girar(dice2)
            tirada(dice2)
        }
    }
    fun girar(dice:ImageView){
        dice.startAnimation(AnimationUtils.loadAnimation(this,R.anim.shake));
    }
    fun tirada(dice:ImageView): Boolean {
        val num=Random.nextInt(0,6)
        dice.setImageResource(daus[num])
        if (num==5){
            return true
        }
        return false
    }
    fun reset(dice:ImageView){
        dice.setImageResource(R.drawable.empty_dice)
    }
}